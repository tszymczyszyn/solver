
class Solver
  def initialize(data_filename)
    @data = read_data(data_filename)
  end

  # Iterate over every row from top to bottom, and within each row from left to right, computing
  # partial sums for every element of the graph. For each element E[i, j] there are up to two edges it
  # can be reached with from higher row, thus sum S[i, j] is equal to V[i, j] + S[i - 1, j] or
  # V[i, j] + S[i - 1, j - 1] (V is value of given element; note that there are multiple possible
  # values of S for every element). 
  # Returns maximum of sums computed for the lowest row of the triangle.
  def run
    sums = [@data[0]] # computed sums for current row

    (1...@data.size).each do |vert_idx|
      sums = process_row(vert_idx, sums)
    end
    return sums.flatten.max
  end

  private

  # Computes sums for a single row taking into account previously computed sums for
  # higher row.
  def process_row(vert_idx, sums)
    row = @data[vert_idx]
    new_sums = []
    row.each_with_index do |value, hor_idx|
      element_sums = []

      if hor_idx > 0
        sums[hor_idx - 1].each { |s| element_sums << (s + value) }
      end

      if hor_idx < vert_idx
        sums[hor_idx].each { |s| element_sums << (s + value) }
      end

      new_sums << element_sums
    end
    return new_sums
  end

  # Reads data into nested array, i.e. [[5], [3, 6], [2, 9, 4]] for three level triangle.
  def read_data(filename)
    data = []
    File.open(filename, "r") do |file|
      file.each_line do |line|
        data << line.strip.split.collect { |node| node.to_i }
      end
    end
    return data
  end

end

%w[data-small data-small-2 data].each do |dataset|
  puts "-------------------------------------------"
  puts "Dataset: #{dataset}"
  puts "Result: #{Solver.new("#{dataset}.txt").run}"
end

